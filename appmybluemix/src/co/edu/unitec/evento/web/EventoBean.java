package co.edu.unitec.evento.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import co.edu.unitec.evento.db.Participante;

public class EventoBean implements Serializable{
	
	private static final long serialVersionUID = 799330289555897062L;

	private String codigo;
	
	private String nombre;
	
    private String apellido;
	
    private String telefono;
    
    private String mensaje;
    
    private String mensajeCelular;
    
    private Boolean mostrarMensaje = new Boolean(false);;
    
    private List<Participante> participantesList;
	    
    private static final String PERSISTENCE_UNIT_NAME = "Participante";
    
    public static final String ACCOUNT_SID = "AC30c988132217de97265a2139b2158464";
    
    public static final String AUTH_TOKEN = "135ac9b87ed55883d44ef68f5a96cf11";

	
	private static EntityManagerFactory factory;
	
    public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@SuppressWarnings("unchecked")
	public List<Participante> getParticipantesList() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery("SELECT p FROM Participante p");
		participantesList = q.getResultList();
		return participantesList;
	}

	public void setParticipantesList(List<Participante> participantesList) {
		this.participantesList = participantesList;
	}

	public void guardarParticipante(){
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		mensaje = null;
		mostrarMensaje = new Boolean(false);
		try{
			//Crear un participante
			em.getTransaction().begin();
			Participante participante = new Participante();
			participante.setCodigo(codigo);
			participante.setNombre(nombre);
			participante.setApellido(apellido);
			participante.setTelefono(telefono);
			em.persist(participante);
			em.getTransaction().commit();
		}catch(Exception ex){
			mensaje = "El participante " + nombre  + " " +
					apellido + " ya existe";
			mostrarMensaje = new Boolean(true);
		}finally{
			//Limpiar los datos
			codigo = null;
			nombre = null;
			apellido = null;
			telefono = null;
			em.close();
		}
		
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Boolean getMostrarMensaje() {
		return mostrarMensaje;
	}

	public void setMostrarMensaje(Boolean mostrarMensaje) {
		this.mostrarMensaje = mostrarMensaje;
	}

	public String getMensajeCelular() {
		return mensajeCelular;
	}

	public void setMensajeCelular(String mensajeCelular) {
		this.mensajeCelular = mensajeCelular;
	}
	
	public void enviarMensaje(){
		try{
			mensaje = null;
			mostrarMensaje = new Boolean(false);
			TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery("SELECT p FROM Participante p");
			participantesList = q.getResultList();
			for (Participante user : participantesList) {
				String telefono = user.getTelefono();
				// Build a filter for the MessageList
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("Body", getMensajeCelular()));
				params.add(new BasicNameValuePair("To", "+57" + telefono));
				params.add(new BasicNameValuePair("From", "+12563735196"));
		 
				MessageFactory messageFactory = client.getAccount().getMessageFactory();
				Message message = messageFactory.create(params);				
			}		 
			setMensajeCelular(null);
		}catch(TwilioRestException tex){
			mensaje = tex.getMessage();
			mostrarMensaje = new Boolean(true);
		}catch(Exception ex){
			mensaje = ex.getMessage();
			mostrarMensaje = new Boolean(true);
		}
	}
}
